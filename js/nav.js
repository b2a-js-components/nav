var navComponent = (function () {

    /*
        <!-- ul.treeView/li/span -->
        <!-- ul.treeView/li/ul/span -->
        <!-- ul.treeView/li/ul/li -->
    */
    
    let firstEl = function(elParent, name) {

        for (let i = 0; i < elParent.children.length; i++) {
            let liChild = elParent.children[i];
            if (liChild.tagName.toUpperCase() === name.toUpperCase()) {
                return liChild;
            }
        }
        
        return null;
    };
    
    let childEls = function(elParent, name) {

        let elements = [];
        for (let i = 0; i < elParent.children.length; i++) {
            let liChild = elParent.children[i];
            if (liChild.tagName.toUpperCase() === name.toUpperCase()) {
                elements.push(liChild);
            }
        }
        
        return elements;
    };
    
    let decorate = function(ul,level){};
    decorate = function(ul, level) {
        ul.classList.add("nav-level-" + level);

        let lis = childEls(ul, "li");
        
        for (let i = 0; i < lis.length; i++) {
            let liChild = lis[i]; 
            liChild.classList.add("nav-level-" + level);

            let ulChild = null;
            let spanChild = firstEl(liChild, "span");
            let aChild = firstEl(liChild, "a");
            
            let ulChilds = childEls(liChild, "ul");
            for (let j = 0; j < ulChilds.length; j++) {
                ulChild = ulChilds[j];
                decorate(ulChild, level + 1);
            }
            

            if (!(spanChild == null  || ulChild == null || ulChild.childElementCount === 0)) {
                spanChild.classList.add("caret");

                ulChild.addEventListener("click", function (e) {
                    e.preventDefault();
                    e.cancelBubble = true;
                    return false;
                });
                
                liChild.addEventListener("click", function () {

                    spanChild.classList.toggle("caret-down");
                    
                    if (level === 0) {
                        liChild.classList.toggle("open");
                    }
                });
            }
            else if (!(spanChild == null)) {
                let spanAChild = firstEl(spanChild, "a");
                if (spanAChild !== null) {
                    liChild.addEventListener("click", function (e) {
                        e.preventDefault();
                        e.cancelBubble = true;
                        spanAChild.click();
                        
                        return true;
                    });
                }
            }
            else if (!(aChild == null  || ulChild != null)) {

                liChild.addEventListener("click", function (e) {
                    e.preventDefault();
                    e.cancelBubble = true;
                    aChild.click();
                    return true;
                });
            }
        }
    };
    
    let changePageHandler = function (args) {

        let items = $('#Nav li').filter(function(i, x) {
            return x.dataset.resourceLocator === args.resourceLocator;
        });

        // console.log(items);
        items.each(function (i, x){

            let target = x;
            do {
                target.classList.add("open");
                let ul = target.parentElement;

                let liElements = childEls(ul, "li");
                for (let ii=0;ii<liElements.length;ii++) {
                    let liElement = liElements[ii];
                    if (liElement !== target) {

                        liElement.classList.remove("open");
                    }
                }

                target = ul.parentElement;
                if (target.tagName.toUpperCase() !== "LI") {
                    target = null;
                }
            } while(target !== null);
        });
    };
    
    return {
        initialize: function (selector, element) {
            let elementTarget = element || document; 
            let navs = elementTarget.querySelectorAll(selector || ".treeView");
            for (let i = 0; i < navs.length; i++) { decorate(navs[i], 0); }
            
            return changePageHandler;
        } 
    };
})();

/*

/ * Style the buttons that are used to open and close the accordion panel * /

.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    text-align: left;
    border: none;
    outline: none;
    transition: 0.4s;
}

/* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) * /
.active, .accordion:hover {
    background-color: #ccc;
}

/ * Style the accordion panel. Note: hidden by default * /
.panel {
    padding: 0 18px;
    background-color: white;
    display: none;
    overflow: hidden;
}
* */